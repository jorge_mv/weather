package de.beowulf.wetter

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import de.beowulf.wetter.adapter.MyListDayCitiesAdapter
import de.beowulf.wetter.databinding.WeatherForecastBinding
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors

class MultiCityActivity : AppCompatActivity() {

    private lateinit var binding: WeatherForecastBinding

    private var add: Boolean = true
    private val gf = GlobalFunctions()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = WeatherForecastBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gf.initializeContext(this)

        binding.Options.visibility = View.VISIBLE

        loadData()

        binding.Add.setOnClickListener {
            binding.Options.visibility = View.GONE
            binding.AddCity.visibility = View.VISIBLE
            binding.City.setAdapter(
                ArrayAdapter(
                    this,
                    android.R.layout.simple_list_item_1,
                    emptyArray<String>()
                )
            )
            add = true
        }

        binding.Remove.setOnClickListener {
            binding.Options.visibility = View.GONE
            binding.AddCity.visibility = View.VISIBLE
            binding.City.threshold = 0
            binding.City.setAdapter(ArrayAdapter(this, R.layout.spinner, gf.getCities()))
            add = false
        }

        binding.Save.setOnClickListener {
            val city = binding.City.text.toString()
            if (add) {
                val executor = Executors.newScheduledThreadPool(5)

                doAsync(executorService = executor) {
                    val result: JSONObject? = try {
                        JSONObject(URL(gf.url("cities", city)).readText(Charsets.UTF_8))
                    } catch (e: Exception) {
                        null
                    }
                    uiThread {
                        if (result != null) {
                            val cities = gf.getCities().toMutableList()
                            cities.add(result.getString("name"))
                            gf.setCities(cities.toTypedArray())
                            binding.City.setText("")
                            binding.Options.visibility = View.VISIBLE
                            binding.AddCity.visibility = View.GONE
                            loadData()
                        } else {
                            Toast.makeText(
                                this@MultiCityActivity,
                                R.string.error_3_start,
                                Toast.LENGTH_SHORT
                            ).show()
                            binding.City.setText("")
                            binding.Options.visibility = View.VISIBLE
                            binding.AddCity.visibility = View.GONE
                        }
                    }
                }
            } else {
                val cities = gf.getCities().toMutableList()
                if (cities.indexOf(city) != -1)
                    cities.remove(city)
                gf.setCities(cities.toTypedArray())
                binding.City.setText("")
                binding.Options.visibility = View.VISIBLE
                binding.AddCity.visibility = View.GONE
                loadData()
            }
        }

        binding.ListView.onItemClickListener =
            AdapterView.OnItemClickListener { _, view, _, _ ->
                val moreInfo = view.findViewById<LinearLayout>(R.id.dayMoreInfo)
                if (moreInfo.visibility == View.GONE) {
                    moreInfo.visibility = View.VISIBLE
                } else {
                    moreInfo.visibility = View.GONE
                }
            }
    }

    private fun loadData() {
        val cities = gf.getCities()

        val cityName = arrayOfNulls<String>(cities.size)
        val statusCity = arrayOfNulls<Int>(cities.size)
        val statusCityText = arrayOfNulls<String>(cities.size)
        val minTemp = arrayOfNulls<String>(cities.size)
        val maxTemp = arrayOfNulls<String>(cities.size)
        val sunrise = arrayOfNulls<String>(cities.size)
        val sunset = arrayOfNulls<String>(cities.size)
        val wind = arrayOfNulls<String>(cities.size)
        val pressure = arrayOfNulls<String>(cities.size)
        val humidity = arrayOfNulls<String>(cities.size)
        val rainSnow = arrayOfNulls<String>(cities.size)
        val rainSnowType = arrayOfNulls<String>(cities.size)

        for (city in cities) {
            val executor = Executors.newScheduledThreadPool(1)

            doAsync(executorService = executor) {
                val result: JSONObject? = try {
                    JSONObject(URL(gf.url("cities", city)).readText(Charsets.UTF_8))
                } catch (e: Exception) {
                    null
                }
                uiThread {
                    if (result == null) {
                        Toast.makeText(
                            this@MultiCityActivity,
                            R.string.error_occurred,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        val i: Int = cities.indexOf(city)
                        val status = result.getJSONArray("weather").getJSONObject(0)
                        val main = result.getJSONObject("main")

                        cityName[i] = result.getString("name")
                        statusCity[i] = gf.icon(status.getString("icon"))
                        statusCityText[i] = status.getString("description")

                        minTemp[i] = gf.convertTemp(main.getDouble("temp_min"), 0)
                        maxTemp[i] = gf.convertTemp(main.getDouble("temp_max"), 0)
                        sunrise[i] =
                            SimpleDateFormat(getString(R.string.time), Locale.ROOT).format(
                                Date(
                                    result.getJSONObject("sys").getLong("sunrise") * 1000
                                )
                            )
                        sunset[i] =
                            SimpleDateFormat(getString(R.string.time), Locale.ROOT).format(
                                Date(
                                    result.getJSONObject("sys").getLong("sunset") * 1000
                                )
                            )
                        val windDegree: Int = result.getJSONObject("wind").getInt("deg")
                        val windDouble: Double = result.getJSONObject("wind").getDouble("speed")
                        wind[i] = gf.convertSpeed(windDouble) + " (${
                            gf.degToCompass(windDegree)
                        })"
                        pressure[i] = main.getString("pressure") + "hPa"
                        humidity[i] = main.getString("humidity") + "%"
                        when {
                            result.has("rain") -> {
                                rainSnow[i] = gf.convertRain(result.getJSONObject("rain").getDouble("1h"))
                                rainSnowType[i] = "rain"
                            }
                            result.has("snow") -> {
                                rainSnow[i] = gf.convertRain(result.getJSONObject("snow").getDouble("1h"))
                                rainSnowType[i] = "snow"
                            }
                            else -> {
                                rainSnow[i] = gf.convertRain(0.0)
                                rainSnowType[i] = "rain"
                            }
                        }

                        val myListAdapter = MyListDayCitiesAdapter(
                            this@MultiCityActivity,
                            cityName,
                            statusCity,
                            statusCityText,
                            minTemp,
                            maxTemp,
                            sunrise,
                            sunset,
                            wind,
                            pressure,
                            humidity,
                            rainSnow,
                            rainSnowType
                        )
                        binding.ListView.adapter = myListAdapter
                    }
                }
            }
        }
        val myListAdapter = MyListDayCitiesAdapter(
            this@MultiCityActivity,
            cityName,
            statusCity,
            statusCityText,
            minTemp,
            maxTemp,
            sunrise,
            sunset,
            wind,
            pressure,
            humidity,
            rainSnow,
            rainSnowType
        )
        binding.ListView.adapter = myListAdapter
    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}