package de.beowulf.wetter

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import java.util.*

class WidgetUpdater(private val mContext: Context, private val Widget: String) {
    private val alarmID = 0
    fun startAlarm() {
        val calendar = Calendar.getInstance()
        val intervalMillis = 900000
        calendar.add(Calendar.MILLISECOND, intervalMillis)
        var alarmIntent = Intent(mContext, AppWidget::class.java)
        when (Widget) {
            "AppWidget" -> {
                alarmIntent.action = AppWidget.ACTION_AUTO_UPDATE
            }
            "TimeWidget" -> {
                alarmIntent = Intent(mContext, TimeWidget::class.java)
                alarmIntent.action = TimeWidget.ACTION_AUTO_UPDATE
            }
        }
        val pendingIntent = PendingIntent.getBroadcast(
            mContext,
            alarmID,
            alarmIntent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
        val alarmManager = mContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        // RTC does not wake the device up
        alarmManager.setRepeating(
            AlarmManager.RTC,
            calendar.timeInMillis,
            intervalMillis.toLong(),
            pendingIntent
        )
    }

    fun stopAlarm() {
        var alarmIntent = Intent(mContext, AppWidget::class.java)
        when (Widget) {
            "AppWidget" -> {
                alarmIntent.action = AppWidget.ACTION_AUTO_UPDATE
            }
            "TimeWidget" -> {
                alarmIntent = Intent(mContext, TimeWidget::class.java)
                alarmIntent.action = TimeWidget.ACTION_AUTO_UPDATE
            }
        }
        val pendingIntent = PendingIntent.getBroadcast(
            mContext,
            alarmID,
            alarmIntent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
        val alarmManager = mContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.cancel(pendingIntent)
    }
}