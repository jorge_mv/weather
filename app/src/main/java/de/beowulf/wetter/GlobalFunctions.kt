package de.beowulf.wetter

import android.content.Context
import android.content.SharedPreferences
import org.json.JSONObject
import java.util.*
import kotlin.math.floor
import kotlin.math.roundToInt

class GlobalFunctions {

    private lateinit var settings: SharedPreferences
    private lateinit var context: Context

    fun initializeContext(con: Context) {
        settings = con.getSharedPreferences("de.beowulf.wetter", 0)
        context = con
    }

    fun degToCompass(num: Int): String {
        val arr: Array<String> = context.resources.getStringArray(R.array.Degree)
        val value: Int = floor((num / 22.5) + 0.5).toInt()
        return arr[(value % 16)]
    }

    fun result(): JSONObject {
        return JSONObject(settings.getString("result", "").toString())
    }

    fun setResult(result: String) {
        val editor: SharedPreferences.Editor = settings.edit()
        editor.putString("result", result)
        editor.putBoolean("initialized", true)
        editor.apply()
    }

    fun url(type: String, city: String): String {
        val api = settings.getString("api", "")
        val langCode: Array<String> = context.resources.getStringArray(R.array.lang_Code)
        val lat = settings.getString("lat", "")!!
        val lon = settings.getString("lon", "")!!

        var lang = "en"
        if (langCode.indexOf(Locale.getDefault().language) != -1) {
            lang = Locale.getDefault().language
        }

        return when (type) {
            "cities" -> {
                "https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$api&lang=$lang"
            }
            "Map" -> {
                "file:///android_asset/map.html?lat=$lat&lon=$lon&appid=$api&zoom=10"
            }
            else -> {
                "https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$lon&exclude=minutely,alerts&appid=$api&lang=$lang"
            }
        }
    }

    fun icon(iconName: String): Int {
        val icon =
            if (!iconName.startsWith("01") && !iconName.startsWith("02") && !iconName.startsWith("10")) {
                "status${iconName.dropLast(1)}"
            } else {
                "status$iconName"
            }
        return context.resources.getIdentifier(icon, "drawable", context.packageName)
    }

    fun getCities(): Array<String> {
        return settings.getStringSet("cities", emptySet())?.toTypedArray() ?: emptyArray()
    }

    fun setCities(cities: Array<String>) {
        val editor: SharedPreferences.Editor = settings.edit()
        editor.putStringSet("cities", cities.toSet())
        editor.apply()
    }

    fun getInitialized(): Boolean {
        return settings.getBoolean("initialized", false)
    }

    fun setInitialized(bool: Boolean) {
        settings.edit()
            .putBoolean("initialized", bool)
            .apply()
    }

    fun unitTemp(): String {
        return " [${context.resources.getStringArray(R.array.unitsTemp)[settings.getInt("unitTemp", 0)]}]:"
    }

    fun graphTemp(temp: Double): Int {
        return when {
            settings.getInt("unitTemp", 0) == 1 -> { //°C
                (temp - 273.15).roundToInt()
            }
            settings.getInt("unitTemp", 0) == 2 -> {//°F
                (temp * 9 / 5 - 459.67).roundToInt()
            }
            else -> { //K
                temp.roundToInt()
            }
        }
    }

    fun convertTemp(temp: Double, round: Int): String {
        val unitTemp =
            context.resources.getStringArray(R.array.unitsTemp)[settings.getInt("unitTemp", 0)]
        return when {
            settings.getInt("unitTemp", 0) == 1 -> { //°C
                String.format("%.${round}f", (temp - 273.15))
            }
            settings.getInt("unitTemp", 0) == 2 -> {//°F
                String.format("%.${round}f", (temp * 9 / 5 - 459.67))
            }
            else -> { //K
                String.format("%.${round}f", temp)
            }
        } + unitTemp
    }

    fun convertSpeed(speed: Double): String {
        val unitSpeed =
            context.resources.getStringArray(R.array.unitsSpeed)[settings.getInt("unitSpeed", 0)]
        return when {
            settings.getInt("unitSpeed", 0) == 1 -> { //km/h
                String.format("%.2f", (speed * 3.6))
            }
            settings.getInt("unitSpeed", 0) == 2 -> {//mph
                String.format("%.2f", (speed * 2.23693629205))
            }
            else -> { //m/s
                String.format("%.2f", speed)
            }
        } + unitSpeed
    }

    fun convertRain(rain: Double): String {
        val unitDistance =
            context.resources.getStringArray(R.array.unitsDistance)[settings.getInt("unitDistance", 0)]
        return when {
            rain == 0.0 -> {
                "0"
            }
            settings.getInt("unitDistance", 0) == 1 -> { //in
                String.format("%.2f", (rain / 25.4))
            }
            else -> { //mm
                String.format("%.2f", rain)
            }
        } + unitDistance
    }
}