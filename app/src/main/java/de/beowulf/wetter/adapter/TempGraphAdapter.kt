package de.beowulf.wetter.adapter

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import de.beowulf.wetter.R

class TempGraphAdapter(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {

    private val dataSet = mutableListOf<DataPoint>()
    private var xMin = 0
    private var xMax = 0
    private var yMin = 0
    private var yNull = 0
    private var yMax = 0

    private val dataPointPaint = Paint().apply {
        color = ContextCompat.getColor(context, R.color.colorAccent)
        strokeWidth = 10f
        style = Paint.Style.STROKE
    }

    private val dataPointFillPaint = Paint().apply {
        color = Color.BLUE
    }

    private val dataPointLinePaint = Paint().apply {
        color = ContextCompat.getColor(context, R.color.colorAccent)
        strokeWidth = 12f
        isAntiAlias = true
    }

    private val axisLinePaint = Paint().apply {
        color = Color.WHITE
        strokeWidth = 5f
    }

    private val textPaint = Paint().apply {
        color = Color.BLACK
        isFakeBoldText = true
        typeface = Typeface.DEFAULT_BOLD
        textSize = 40f
    }

    private val textRightPaint = Paint().apply {
        color = Color.BLACK
        isFakeBoldText = true
        textAlign = Paint.Align.RIGHT
        typeface = Typeface.DEFAULT_BOLD
        textSize = 40f
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        dataSet.forEachIndexed { index, currentDataPoint ->
            val realX = currentDataPoint.xVal.toRealX()
            val realY = (currentDataPoint.yVal + yNull).toRealY()

            if (index < dataSet.size - 1) {
                val nextDataPoint = dataSet[index + 1]
                val endX = nextDataPoint.xVal.toRealX()
                val endY = (nextDataPoint.yVal + yNull).toRealY()
                canvas.drawLine(realX, realY, endX, endY, dataPointLinePaint)
            }

            canvas.drawLine(realX, realY, realX, 0f, axisLinePaint)
            canvas.drawCircle(realX, realY, 19f, dataPointFillPaint)
            canvas.drawCircle(realX, realY, 19f, dataPointPaint)
            canvas.save()
            canvas.scale(1f, -1f, realX, 0f)
            if (index != 7) {
                canvas.drawText(currentDataPoint.yVal.toString(), realX+2f, -15f, textPaint)
            } else {
                canvas.drawText(currentDataPoint.yVal.toString(), realX-2f, -15f, textRightPaint)
            }
            canvas.restore()
            canvas.save()
        }

        canvas.drawLine(2.5f, 0f, 2.5f, height.toFloat(), axisLinePaint)
        if (yNull == 0) {
            canvas.drawLine(0f, 2.5f, width.toFloat(), 2.5f, axisLinePaint)
        } else {
            canvas.drawLine(0f, yNull.toRealY()-2.5f, width.toFloat(), yNull.toRealY()-2.5f, axisLinePaint)
        }
    }

    fun setData(newDataSet: List<DataPoint>) {
        xMin = newDataSet.minByOrNull { it.xVal }?.xVal ?: 0
        xMax = newDataSet.maxByOrNull { it.xVal }?.xVal ?: 0
        yMin = newDataSet.minByOrNull { it.yVal }?.yVal ?: 0
        yMax = newDataSet.maxByOrNull { it.yVal }?.yVal ?: 0

        if (yMin < 0) {
            yMax -= yMin
            yNull = yMin * -1
            yMin = 0
        }
        dataSet.clear()
        dataSet.addAll(newDataSet)
        invalidate()
    }

    private fun Int.toRealX() = toFloat() / xMax * width
    private fun Int.toRealY() = toFloat() / yMax * height
}

data class DataPoint(
    val xVal: Int,
    val yVal: Int
)