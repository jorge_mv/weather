- added weather maps
- new standard API-Key (thx to OpenWeatherMap)
- improved error messages during setup
- Finnish translation added (thx @Nekromanser)
- Units added to graphs
- Typos fixed
