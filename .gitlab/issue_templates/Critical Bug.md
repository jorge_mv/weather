<!-- Only use this form if it is a critical bug! For example, the app crashes... -->

### Summary
- Summarize the bug encountered concisely

### Steps to reproduce
1. 
2. 
3. 

### Expected behaviour
- Tell us what should happen

### Actual behaviour
- Tell us what happens

### Environment data
- Android version:

- Device model: 

- Stock or customized system:

- Weather app version:

### Relevant  screenshots
- Paste any relevant screenshots

/label ~critical ~bug 
/cc @BeowuIf 
